#!/bin/sh
## Parallel MC simulation using snow

#$ -N faulty_rng
#$ -o faulty_rng.log
#$ -j y ## Combining output/error messages into one file
#$ -cwd
#$ -pe mpi 10

R --vanilla < rng_parallel_faulty.R

exit 0

