##' ------------------------------------
##' Random Number Generation
##'
##' Exercise:
##' The following script can be used to generate random numbers
##' in parallel. But there is a big mistake in the script.
##' 1) Run the faulty script and save the resaults.
##' 2) Find the mistake and correct it.
##' 3) Run the corrected script and compare the resaults.
##' ------------------------------------
require("parallel")

ntasks <- as.integer(Sys.getenv("SLURM_NTASKS", 0)) - 1L
stopifnot(ntasks > 0L)

# start MPI cluster
clu <- makeCluster(ntasks, type = "PSOCK")

clusterEvalQ(clu, set.seed(1))
fun <- function(x) mapply(function(x) runif(2), 1:100)

x <- parLapply(clu, 1:100, fun)

saveRDS(x, file="bad_rng_snow.rds")

# stopCluster(cl)  ## freezes
